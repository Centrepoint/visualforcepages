<apex:page standardController="Contact" 
    showHeader="true" 
    sidebar="true" 
    title="Create a Tenancy on Castleton" 
    cache="false">
    <head>
    <apex:stylesheet value="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"  />
    <apex:includeScript value="https://code.jquery.com/jquery-1.9.1.js" />
    <apex:includeScript value="https://code.jquery.com/ui/1.10.3/jquery-ui.js" />    
    <script>
        // set up the date picker
        j$ = jQuery.noConflict();
        j$(document).ready(function() {
            // call functions to set up datepickers, the progress bar and UI actions
            fadeInClientInformatin();
            createStartDateBindings();
            createEndDateBindings();
            setUpProgressBar();
            setUpProgressBar2();
            disableCreateButton();
        });   
        // fade in client information
        function fadeInClientInformatin() {
            j$( "#ClientInformation" ).fadeIn( 1750 )        
        }     
        // create start date bindings     
        function createStartDateBindings() {  
            j$( "#tenancy_start_date" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                dateFormat:"dd-mm-yy",
                altField: "#tenancy_start_date_alt",
                altFormat: "yy-mm-dd",
                showAnim: "slide"
            })            
        }    
        // create end date bindings
        function createEndDateBindings() {
            j$( "#tenancy_end_date" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                dateFormat:"dd-mm-yy",
                altField: "#tenancy_end_date_alt",
                altFormat: "yy-mm-dd",
                showAnim: "slide"
            })  
        }
        function setUpProgressBar() {
            j$( "#progressbar" ).progressbar({
                value: false
            }) 
        }
        function setUpProgressBar2() {
            j$( "#progressbar2" ).progressbar({
                value: false
            }) 
        }
        function disableCreateButton() {
            j$('.yesBtn').prop( "disabled", true );
            j$('.yesBtn').css( "cursor", "not-allowed" );
            j$('.yesBtn').addClass( "pyr-btn-gray" );    
        }
        // do not set the focus to the first field on the page
        function setFocusOnLoad() {}    
        // custom lookup functionality
        var newWin=null;
        function openLookupPopup(name, id)
        {
            var url="/apex/KyperaPropertiesLookupPopup?namefield=" + name + "&idfield=" + id;
            newWin=window.open(url, 'Popup','height=500,width=600,left=100,top=100,resizable=no,scrollbars=yes,toolbar=no,status=no');
            if (window.focus) 
            {
                newWin.focus();
            }
                
            return false;
        }     
        function closeLookupPopup()
        {
           if (null!=newWin)
           {
              newWin.close();
           }  
        }
    </script> 
    <style>
        .mainVFPageTitle {
            color: #666;
            font-size: 2.0em;
            font-family: Verdana, Geneva, sans-serif;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 10px;
            text-align: center;
            display: block;
        }
        .ClientInformation {
            display: none;
        }
        .Stage1Container {
            border: 2px solid;
            min-height: 150px;
            border-color: grey;
            border-radius: 10px;
            margin-bottom: 20px;
            color: #666;
        }        
        .Stage2Container {
            border: 2px solid;
            min-height: 175px;
            border-color: grey;
            border-radius: 10px;
            margin-bottom: 20px;
            color: #666;
        } 
        .Stage3Container {
            border: 2px solid;
            height: 175px;
            border-color: grey;
            border-radius: 10px;
            margin-bottom: 20px;
            color: #666;
        } 
        .StageTitle {
            color: #666;
            font: 14px Arial, sans-serif;        
        } 
        .blockLookupLink {
            display: inline-block;
            vertical-align: middle;
            padding-right: 20px;
        }
        .resetLink{
            padding-left: 15px;
        }
        .Stage1_Outer {
            margin: 20px;
        }     
        .Stage2_Outer {
            margin: 20px;
        }     
        .Stage3_Outer {
            margin: 20px;
        }   
        .marginRight20px {
            margin-right: 20px;
        } 
        .marginRight5px {
            margin-right: 5px;
        }
        .AvailabilityProgress {
            display: block;
            margin-top: 5px;
            color:grey;
            font-size:1.5em;*/
        }
        .CreateProgress {
            display: block;
            margin-top: 5px;
            color:red;
            font-size:1.5em;*/
        }  
        /* first progress bar */      
        #progressbar .ui-progressbar-value {
            background-color: #99CCFF;
            height: 15px;
        }
        #progressbar {
            height: 15px;
        }
        #progressbarTitle {
            padding-left: 33%;
        }     
        /* second progress bar */    
        #progressbar2 .ui-progressbar-value {
            background-color: #FF0000;
            height: 15px;
        }
        #progressbar2 {
            height: 15px;
        }
        #progressbarTitle2 {
            padding-left: 33%;
        } 
        #KyperaTransactions {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            width: 100%;
            border-collapse: collapse;
        }
        
        #KyperaTransactions td, #KyperaTransactions th {
            font-size: 1em;
            border: 1px solid rgb(137, 207, 207);
            padding: 3px 7px 2px 7px;
        }
        
        #KyperaTransactions th {
            font-size: 1em;
            text-align: left;
            padding-top: 5px;
            padding-bottom: 4px;
            background-color: rgb(23, 151, 192);
            color: #ffffff;
        }
        
        #KyperaTransactions tr.alt td {
            color: #000000;
            background-color: rgb(233, 240, 243);
        }
        #kyperaTransactionsCount {
            font-weight: bold;
        }       
    </style> 
    </head>  
    <div id="mainVFPageTitle" class="mainVFPageTitle">Create a Tenancy</div>
    <apex:pageMessages id="msgs" />
    <div id="ClientInformation" class="ClientInformation">
    <apex:pageBlock title="Client Information">
        <apex:pageBlockSection columns="1" >
            <apex:pageBlockSectionItem dataStyle="width:33%" labelStyle="width:10%">
                <apex:outputLabel value="Client" />
                <apex:outputLink value="/{!Contact.Id}" title="Click here to go back to the Client record">{!Contact.Name}</apex:outputLink>
            </apex:pageBlockSectionItem>
        <apex:pageBlockSectionItem >
                <apex:outputLabel value="First Name" />
                <apex:outputText value="{!Contact.FirstName}"  />
        </apex:pageBlockSectionItem>         
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Last Name" />
                <apex:outputText value="{!Contact.LastName}" />
        </apex:pageBlockSectionItem>
        </apex:pageBlockSection>  
    </apex:pageBlock>
    </div>
    <apex:form id="myForm">
    
    <!-- Stage 1: find a property using the date or the unit or both-->
    <apex:outputPanel id="Stage1Container"
        styleClass="Stage1Container"
        layout="block">
        
        <apex:outputPanel id="Stage1_Outer"
            styleClass="Stage1_Outer"
            layout="block">
            
            <span class="StageTitle">1. Select a Timeline Event.</span>
            <br />
            <br />  
            <apex:outputPanel id="tableContainer" rendered="true">
                 <table id="KyperaTransactions" width="100%">
                 <th>Select a Timeline Event</th><th>Lead project</th><th>Date</th>
                     <tr>
                         <td>
                             <apex:commandButton value="TE-50996" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="Hostel House"/>
                         </td>
                         <td>
                            <apex:outputText value="01/02/2015"/>
                         </td>
                     </tr> 
                     <tr>
                         <td>
                             <apex:commandButton value="TE-50946" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="Accommodation Park"/>
                         </td>
                         <td>
                            <apex:outputText value="27/03/2015"/>
                         </td>
                     </tr> 
                     <tr>
                         <td>
                             <apex:commandButton value="TE-50974" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="Residential Place"/>
                         </td>
                         <td>
                            <apex:outputText value="06/04/2015"/>
                         </td>
                     </tr>     
                 </table>
                 <br />
                 3 Timeline Events
            </apex:outputPanel>
            <div id="progressbarTitle" style="font-size: 1.5em;">Checking property availability...</div> 
                    <div id="progressbar"></div>     
            
        </apex:outputPanel>
        
    </apex:outputPanel>

    <!-- Stage 2: select a property from the returned list -->
    <apex:outputPanel id="Stage2Container"
        styleClass="Stage2Container"
        layout="block">
        <apex:outputPanel id="Stage2_Outer"
            styleClass="Stage2_Outer"
            layout="block">
            <span class="StageTitle">2. Select a property.</span>
            <br />
            <br />  
            <apex:outputPanel id="tableContainer2" rendered="true">
                 <table id="KyperaTransactions" width="100%">
                 <th>Select a Unit</th><th>Unit Reference</th><th>Unit Name</th><th>Block Name</th><th>Block Reference</th>
                     <tr>
                         <td>
                             <apex:commandButton value="HH001" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="HH001"/>
                         </td>
                         <td>
                            <apex:outputText value="HostelHouse1"/>
                         </td>
                         <td>
                            <apex:outputText value="Hostel House"/>
                         </td>
                         <td>
                            <apex:outputText value="HH1"/>
                         </td>
                     </tr>  
                     <tr>
                         <td>
                             <apex:commandButton value="HH002" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="HH002"/>
                         </td>
                         <td>
                            <apex:outputText value="HostelHouse2"/>
                         </td>
                         <td>
                            <apex:outputText value="Hostel House"/>
                         </td>
                         <td>
                            <apex:outputText value="HH2"/>
                         </td>
                     </tr> 
                     <tr>
                         <td>
                             <apex:commandButton value="HH003" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="HH003"/>
                         </td>
                         <td>
                            <apex:outputText value="HostelHouse3"/>
                         </td>
                         <td>
                            <apex:outputText value="Hostel House"/>
                         </td>
                         <td>
                            <apex:outputText value="HH3"/>
                         </td>
                     </tr>   
                     <tr>
                         <td>
                             <apex:commandButton value="HH005" 
                                styleClass="marginRight20px " 
                                reRender="techResultsPanel, Stage3Container"
                                oncomplete="createEndDateBindings(), setUpProgressBar2()">
                            </apex:commandButton>
                         </td>
                         <td>
                             <apex:outputText value="HH005"/>
                         </td>
                         <td>
                            <apex:outputText value="HostelHouse5"/>
                         </td>
                         <td>
                            <apex:outputText value="Hostel House"/>
                         </td>
                         <td>
                            <apex:outputText value="HH5"/>
                         </td>
                     </tr>                        
                 </table>
                 <br />
                 4 Vacant Properties
            </apex:outputPanel>            
        </apex:outputPanel>
    </apex:outputPanel>    
    
    <!-- Stage 3: optionally enter an end date and create the tenancy -->
    <apex:outputPanel id="Stage3Container"
        styleClass="Stage3Container"
        layout="block">
        <apex:outputPanel id="Stage3_Outer"
            styleClass="Stage3_Outer"
            layout="block"> 
            <span class="StageTitle">3. Enter an (optional) end date and create the tenancy.</span>
            <br />
            
            <!-- optional tenancy end date -->
            <br />
            <label for="tenancy_end_date">Tenancy end date (optional): </label>
            <input type="text" name="tenancy_end_date" id="tenancy_end_date" readonly="readonly" />
            <input type="hidden" name="tenancy_end_date_alt" id="tenancy_end_date_alt" readonly="readonly" />  
            <label for="unit_reference_for_tenancy_creation">Selected Unit: </label>
            <input type="text" 
                   name="unit_reference_for_tenancy_creation" 
                   id="unit_reference_for_tenancy_creation" 
                   value="HH002" readonly="readonly" 
                   size="10" />   
            <input type="text" 
                   name="unit_name_for_tenancy_creation" 
                   id="unit_name_for_tenancy_creation" 
                   value="" readonly="readonly" 
                   class="marginRight20px"
                   size="10" />                    
            <apex:commandButton action="{!save}" 
                    value="Create Tenancy"
                    rerender="resultsPanel, techResultsPanel, msgs, msgs2"
                    status="CreateProgress"
                    styleClass="yesBtn"
                    onclick="disableCreateButton();"
                    />
            <br />      
            <!-- progress bar -->
            <apex:actionStatus id="CreateProgress" 
                startStyleClass="CreateProgress" 
                stopStyleClass="CreateProgress" >
                <apex:facet name="start">
                    <div id="progressbarTitle2">Creating tenancy...</div> 
                    <div id="progressbar2"></div>   
                </apex:facet>                
            </apex:actionStatus>                    
            <apex:outputPanel id="resultsPanel2" >
                    <apex:outputPanel id="resultsPanel">
                        <apex:outputText style="color:red; font-size:1.5em;" 
                            value=""/>
                    </apex:outputPanel>                                       
            </apex:outputPanel> 
        </apex:outputPanel>
    
    </apex:outputPanel>   

    </apex:form>  
    
    <apex:outputPanel id="msgs2Container">
        <apex:pageMessages id="msgs2" />    
    </apex:outputPanel>
       
</apex:page>